<?php
/**
 * Wadachi FuelPHP Toast Package
 *
 * Display Android-like toast messages in the browser.
 *
 * @package    wi-toast
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

\Autoloader::add_classes([
  'Wi\\Interface_Controller_Toast' => __DIR__.'/classes/interface/controller/toast.php',
  'Wi\\Trait_Controller_Toast' => __DIR__.'/classes/trait/controller/toast.php',
]);
