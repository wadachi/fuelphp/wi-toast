<?php
/**
 * Wadachi FuelPHP Toast Package
 *
 * Display Android-like toast messages in the browser.
 *
 * @package    wi-toast
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

namespace Wi;

/**
 * トースト　コントローラ　インターフェース
 */
interface Interface_Controller_Toast
{
  /**
   * トーストのHTML要素ID
   */
  const TOAST_HTML_ID = 'toast-data';

  /**
   * トーストのセッションID
   */
  const TOAST_SESSION_ID = 'toast-data';

  /**
   * トーストのビュー変数名
   */
  const TOAST_VIEW_VAR = '_toast';
}
