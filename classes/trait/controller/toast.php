<?php
/**
 * Wadachi FuelPHP Toast Package
 *
 * Display Android-like toast messages in the browser.
 *
 * @package    wi-toast
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

namespace Wi;

/**
 * コントローラのトースト形質
 */
trait Trait_Controller_Toast
{
  /**
   * アクション後処理
   * リダイレクトされた時に実行されていないのは
   *
   * @access public
   * @param Fuel\Core\Response $response レスポンス
   * @return Fuel\Core\Response レスポンス
   */
  public function after($response)// <editor-fold defaultstate="collapsed" desc="...">
  {
    if (isset($this->template->content))
    {
      // トースト データ
      $toast =
        '<script id="'.self::TOAST_HTML_ID.'" type="application/json">'.
          json_encode(\Session::get_flash(self::TOAST_SESSION_ID, []), JSON_HEX_QUOT).
        '</script>';

      \View::set_global(self::TOAST_VIEW_VAR, $toast, false);
    }

    return parent::after($response);
  }// </editor-fold>

  /**
   * ページにトースト メッセージを追加する
   *
   * @access public
   * @param string $text メッセージ テキスト
   * @param string $type メッセージ 種類（alert, information, warning, error, success）
   */
  protected function toast($text, $type = 'alert')// <editor-fold defaultstate="collapsed" desc="...">
  {
    $toast = \Session::get_flash(self::TOAST_SESSION_ID, []);
    array_push($toast, ['text' => $text, 'type' => $type]);
    \Session::set_flash(self::TOAST_SESSION_ID, $toast);
  }// </editor-fold>
}
